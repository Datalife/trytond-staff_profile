import argparse


def parse_commandline():
    options = {}

    parser = argparse.ArgumentParser(prog='set_state_load_ready')

    parser.add_argument("-c", "--config", dest="configfile", metavar='FILE',
        default=None, help="specify config file")
    parser.add_argument("-d", "--database", dest="database_name", default=None,
        metavar='DATABASE', help="specify the database name")
    parser.add_argument("-l", "--limit", dest="limit", default=None,
        metavar='LIMIT', help="specify the limit number of records")
    options = parser.parse_args()

    if not options.configfile:
        parser.error('Missing config file')
    if not options.database_name:
        parser.error('Missing database option')
    return options


if __name__ == '__main__':

    options = parse_commandline()

    from trytond.transaction import Transaction
    from trytond.pool import Pool
    from trytond.config import config as CONFIG
    CONFIG.update_etc(options.configfile)
    Pool.start()
    pool = Pool(options.database_name)
    pool.init()
    context = {
        'company': 1,
    }

    with Transaction().start(options.database_name, 0, context=context) as t:
        pool = Pool()
        Line = pool.get('timesheet.line')

        line_table = Line.__table__()
        cursor = Transaction().connection.cursor()
        print('>>> START')
        while True:
            lines = Line.search([
                    ('payroll_cost_price', '=', None),
                    ('employee', '!=', None)],
                order=[('date', 'DESC')],
                limit=options.limit)
            if not lines:
                break
            res = {l.id: l.employee.compute_payroll_price(l.date)
                for l in lines}
            for id_, value in res.items():
                cursor.execute(*line_table.update(
                    [line_table.payroll_cost_price], [value],
                    where=(line_table.id == id_)))
            Transaction().commit()
            print('>>> END ITERATION')
        print('>>> END')
