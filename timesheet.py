# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from sql import Column
from sql.aggregate import Sum
from sql.functions import Extract

from trytond import backend
from trytond.model import fields
from trytond.modules.timesheet_cost.company import price_digits
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction


class Timesheet(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    payroll_cost_price = fields.Numeric('Payroll cost price',
        digits=price_digits, readonly=True)

    @classmethod
    def sync_cost(cls, lines):
        super().sync_cost(lines)
        to_write = []
        lines = cls.browse(lines)
        for line in lines:
            if line.employee:
                payroll_cost_price = \
                    line.employee.compute_payroll_price(line.date)
                if payroll_cost_price != line.payroll_cost_price:
                    to_write.extend([[line],
                        {'payroll_cost_price': payroll_cost_price}])
        if to_write:
            cls.write(*to_write)


class TimesheetCostInfo(metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    payroll_cost = fields.Function(
        fields.Numeric('Payroll Cost',
            digits=(16, Eval('cost_amount_currency_digits', 2))),
        'get_payroll_cost')

    @classmethod
    def get_payroll_cost(cls, records, name):
        res = {}
        for record in records:
            amount = Decimal(str(record.hours)) * (
                    record.payroll_cost_price or Decimal('0.0'))
            if record.employee.company.currency:
                amount = record.employee.company.currency.round(amount)
            res[record.id] = amount
        return res

    def compute_payroll_cost(self):
        if self.employee:
            employee = self.employee
            with Transaction().set_context(date=self.date):
                return (Decimal(str(self.hours)) * (
                    employee.get_payroll_price() or Decimal('0.0')
                    ) if self.employee else Decimal('0.0'))
        return Decimal('0.0')


class PayrollTimesheetMixin(object):
    __slots__ = ()

    payroll_amount = fields.Numeric('Payroll amount',
        digits=(16, Eval('currency_digits', 2)))

    @classmethod
    def table_query(cls):
        query = super().table_query()
        line = (query.from_[0] if isinstance(query.from_[0].left, Column)
            else query.from_[0].left)
        columns = list(query.columns)
        if backend.name != 'sqlite':
            columns.append(
                Sum(line.payroll_cost_price
                    * (Extract('HOURS', line.duration)
                        + (Extract('DAYS', line.duration) * 24)
                        + (Extract('MINUTE', line.duration) / 60))
                    ).as_('payroll_amount'))
        else:
            columns.append(
                Sum(line.payroll_cost_price * (line.duration / 3600)
                    ).as_('payroll_amount'))
        query.columns = columns
        return query


class HoursEmployee(PayrollTimesheetMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.hours_employee'


class HoursEmployeeWeekly(PayrollTimesheetMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.hours_employee_weekly'


class HoursEmployeeMonthly(PayrollTimesheetMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.hours_employee_monthly'
