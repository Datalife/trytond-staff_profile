# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, DeactivableMixin
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.config import config
from trytond.pyson import Eval
import datetime

price_digits = (16, config.getint(
        'timesheet_cost', 'price_decimal', default=4))


class Profile(DeactivableMixin, ModelView, ModelSQL):
    """Staff Profile"""
    __name__ = 'staff.profile'

    code = fields.Char('Code')
    name = fields.Char('Name', required=True, select=True)
    company = fields.Many2One('company.company', 'Company')
    payroll_prices = fields.One2Many('staff.profile.payroll_price', 'profile',
        'Payroll Prices')

    @classmethod
    def __register__(cls, module_name):
        super(Profile, cls).__register__(module_name)

        table_h = cls.__table_handler__(module_name)
        table_h.not_null_action('company', 'remove')

    @staticmethod
    def default_company():
        return Transaction().context.get('company', None)


class ProfilePayrollCost(ModelView, ModelSQL):
    """ProfilePayrollPrice"""
    __name__ = 'staff.profile.payroll_price'

    date = fields.Date('Date', select=True, required=True)
    payroll_price = fields.Numeric('Payroll Price', required=True,
        digits=price_digits)
    cost_price = fields.Numeric('Cost Price', required=True,
        digits=price_digits)
    profile = fields.Many2One('staff.profile', 'Profile', select=True)


class EmployeeCostPrice(metaclass=PoolMeta):
    __name__ = 'company.employee_cost_price'

    payroll_price = fields.Numeric('Payroll Price', digits=price_digits)


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    profile = fields.Many2One('staff.profile', 'Profile', select=True,
        domain=[('company', 'in', (None, Eval('company', None)))])
    payroll_price = fields.Function(
        fields.Numeric('Payroll Price'), 'get_payroll_price')

    def get_payroll_price(self, name=None):
        payroll_date = Transaction().context.get('date', None)
        return self.compute_payroll_price(payroll_date)

    def get_employee_prices(self, date=None):
        pool = Pool()
        if not self.profile:
            return 0
        PayrollPrice = pool.get('staff.profile.payroll_price')
        payroll_prices = PayrollPrice.search([
            ('profile', '=', self.profile),
        ], order=[('date', 'ASC')])

        return self.get_payroll_employee_costs(payroll_prices, date)

    def get_payroll_employee_costs(self, payroll_prices, date=None):
        employee_costs = []
        for payroll_price in payroll_prices:
            employee_costs.append(
                (payroll_price.date, payroll_price.payroll_price,
                    payroll_price.cost_price))
        return employee_costs

    def compute_payroll_price(self, date=None):
        pool = Pool()
        Date = pool.get('ir.date')

        profile_price = 0
        profile_edate = datetime.date(1000, 1, 1)
        edate = datetime.date(1000, 1, 1)
        if date is None:
            date = Date.today()

        if self.profile:
            employee_prices = self.get_employee_prices(date)

            if employee_prices and date >= employee_prices[0][0]:
                for profile_edate, eprice, _ in employee_prices:
                    if profile_edate and date >= profile_edate:
                        profile_price = eprice
                    else:
                        break

        price = 0
        for eprice in self.cost_prices[::-1]:
            if date >= eprice.date:
                price = eprice.payroll_price
                edate = eprice.date
            else:
                break

        return price if edate >= profile_edate else profile_price

    def compute_cost_price(self, date=None):
        pool = Pool()
        Date = pool.get('ir.date')

        profile_price = 0
        profile_edate = datetime.date(1000, 1, 1)
        edate = datetime.date(1000, 1, 1)
        if date is None:
            date = Date.today()

        if self.profile:
            employee_prices = self.get_employee_prices(date)

            # compute the cost price for the given date
            if employee_prices and date >= employee_prices[0][0]:
                for profile_edate, _, profile_ecost_price in employee_prices:
                    if profile_edate and date >= profile_edate:
                        profile_price = profile_ecost_price
                    else:
                        break
        employee_costs = self.get_employee_costs()

        cost = 0
        if employee_costs and date >= employee_costs[0][0]:
            for edate, ecost in employee_costs:
                if date >= edate:
                    cost = ecost
                else:
                    break
        return cost if edate >= profile_edate else profile_price
