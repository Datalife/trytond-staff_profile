# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class TimeClockPrintStart(metaclass=PoolMeta):
    __name__ = 'company.employee.time_clock.print_start'

    profile = fields.Many2One('staff.profile', 'Profile')


class TimeClockPrint(metaclass=PoolMeta):
    __name__ = 'company.employee.time_clock.print'

    def _get_timeclock_domain(self):
        res = super()._get_timeclock_domain()
        if self.start.profile:
            res.append(('employee.profile', '=', self.start.profile.id))
        return res
